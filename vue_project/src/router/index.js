import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta:{title:"首页",keepAlive:true}
  },
  {
    path: '/address',
    name: 'Address',
    meta:{title:"地址",keepAlive:false},
    component: () => import(/* webpackChunkName: "about" */ '../views/Address.vue')
  }, 
  {
    path: '/shopping',
    name: 'Shopping',
    meta:{title:"购物车",keepAlive:false},
    component: () => import(/* webpackChunkName: "about" */ '../views/Shopping.vue')
  },
  {
    path: '/order',
    name: 'Order',
    meta:{title:"订单",keepAlive:false},
    component: () => import(/* webpackChunkName: "about" */ '../views/Order.vue')
  },{
    path: '/news',
    name: 'News',
    meta:{title:"新闻资讯",keepAlive:false},
    component: () => import(/* webpackChunkName: "about" */ '../views/News.vue')
  },
  {
    path: '/imgtext',
    name: 'Imgtext',
    meta:{title:"图文专区",keepAlive:false},
    component: () => import(/* webpackChunkName: "about" */ '../views/Imgtext.vue')
  },
  {
    path: '/shoplist',
    name: 'Shoplist',
    meta:{title:"商品列表",keepAlive:false},
    component: () => import(/* webpackChunkName: "about" */ '../views/Shoplist.vue')
  },
  {
    path: '/shopdetail/:id',
    name: 'Shopdetail',
    meta:{title:"商品详情",keepAlive:false},
    component: () => import(/* webpackChunkName: "about" */ '../views/Shopdetail.vue')
  },
  {
    path: '/newsdetail/:id',
    name: 'Newsdetail',
    meta:{title:"资讯详情",keepAlive:false},
    component: () => import(/* webpackChunkName: "about" */ '../views/Newsdetail.vue')
  },
  {
    path: '/imagetextdetail',
    name: 'Imagetextdetail',
    meta:{title:"图文详情",keepAlive:false},
    component: () => import(/* webpackChunkName: "about" */ '../views/Imagetextdetail.vue')
  },
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
