import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

import axios from "axios"
Vue.prototype.axios=axios;



import { 
  Swipe, SwipeItem,Lazyload,Icon,PullRefresh,Loading,
  GoodsAction,GoodsActionIcon,GoodsActionButton,Stepper,Tabbar,TabbarItem,SubmitBar,Button,Checkbox,CheckboxGroup,Card,AddressList
} from 'vant';



Vue.use(AddressList);
Vue.use(SubmitBar);
Vue.use(Tabbar);
Vue.use(TabbarItem);
Vue.use(Loading);
Vue.use(PullRefresh);

Vue.use(Icon);
Vue.use(Swipe);
Vue.use(SwipeItem);
Vue.use(Lazyload);
Vue.use(GoodsAction); 
Vue.use(GoodsActionIcon); 
Vue.use(GoodsActionButton); 
Vue.use(Stepper);
Vue.use(Button);
Vue.use(Checkbox);
Vue.use(CheckboxGroup);
Vue.use(Card);



import VueAwesomeSwiper from "vue-awesome-swiper";
import "swiper/dist/css/swiper.css";
Vue.use(VueAwesomeSwiper);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
