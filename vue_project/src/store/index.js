import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    shopObj:[],
    addressList:[],
  },
  mutations: {
    addAddress(state,data){

    },
    addShop(state,data){
      const sameId=state.shopObj.findIndex(item=>{
        return item.id==data.id;
      })
      sameId==-1?state.shopObj.push(data):state.shopObj[sameId].good_num+=data.good_num;
    }
  },
 getters:{
   shopCount(state){
     let sum=0;
     state.shopObj.forEach(item=>{
       sum+=item.good_num;
     })
     return sum;
   }
 }
})
