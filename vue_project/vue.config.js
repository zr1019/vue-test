module.exports = {
  devServer: {
    port: 4000,
    proxy: {
      "/news/": {
        target: "https://3g.163.com",
        changeOrigin: true,
      },
      //   http://baobab.kaiyanapp.com/api/v5/index/tab/allRec?page=<page_num>
      "/api/v5/": {
        target: "http://baobab.kaiyanapp.com",
        changeOrigin: true,
      },
      
    },
  },
};
